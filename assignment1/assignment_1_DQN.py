import gym
import pylab
import numpy as np
import random
from collections import deque
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam

class mfril:
    def __init__(self,env):
        self.env = env
        self.memory = deque(maxlen=1000000)

        self.gamma = 0.99
        self.epsilon = 1.0
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.999
        self.tau = 0.125
        self.learn_rate = 0.001

        self.batch =  64

        self.main_net = self.createNet()   #model net
        self.target_net = self.createNet() #target_net

        self.min_reward = -200
        self.max_reward_emph =  100 #emphasize reward, if reached the goal

        self.min_reward_env = -100 #from the environment code
        self.max_reward_env =  300 #from the environment code
        self.count_step = 0
        self.acc_list, self.val_acc_list, self.loss_list, self.val_los_list, self.steps_list = [] , [] , [] , [] , []

    def createNet(self):
        print("net create")
        net   = Sequential()
        state_shape  = self.env.observation_space.shape #in our case it is 24
        net.add(Dense(24, input_dim = state_shape[0], activation="relu",kernel_initializer='he_uniform'))
        net.add(Dense(72, activation="relu",kernel_initializer='he_uniform'))
        #net.add(Dense(216, activation="relu",kernel_initializer='he_uniform'))
        net.add(Dense(90, activation="relu",kernel_initializer='he_uniform'))
        net.add(Dense(30, activation="linear",kernel_initializer='he_uniform')) # as we have 30 predef actions
        net.compile(loss="mse", optimizer=Adam(lr=self.learn_rate))
        return net

    def remember(self, state, act_ind, reward, new_state, done):
        self.memory.append([state, act_ind, reward, new_state, done])

    def normalize_reward(self, in_reward):
        #normalizing reward to [0,1]
        #where 0 = self.min_reward
        #      +1 = self.max_reward_env+self.max_reward_emph
        return (in_reward-self.min_reward)/(self.max_reward_env+self.max_reward_emph-self.min_reward)


    def trainMainNet(self):
        if (len(self.memory)<10000):
            return 0
        train_samples = random.sample(self.memory, self.batch)

        initial_states  = np.zeros((self.batch,(self.env.observation_space.shape)[0]))
        update_target = np.zeros((self.batch,(self.env.observation_space.shape)[0]))
        action_indexes, rewards, dones = [], [], []

        for i in range(self.batch):
            initial_states[i] = train_samples[i][0]     #states
            action_indexes.append(train_samples[i][1])  #action_indexes
            rewards.append(train_samples[i][2])         #rewards
            update_target[i] = train_samples[i][3]      #new_states
            dones.append(train_samples[i][4])           #dones

        predict_rewards = self.main_net.predict(initial_states)
        predict_Qs = self.target_net.predict(update_target)

        for i in range(self.batch):
            if dones[i]:
                predict_rewards[i][action_indexes[i]]=rewards[i]
            else:
                predict_rewards[i][action_indexes[i]]=rewards[i]+self.gamma*(np.amax(predict_Qs[i]))

        self.main_net.fit(initial_states,predict_rewards, batch_size=self.batch, epochs=1, verbose=0)

        return np.max(predict_Qs)
        # print(type(history.history['acc']))
        # self.acc_list.append(history.history['acc'][0])
        # self.val_acc_list.append(history.history['val_acc'][0])
        # self.loss_list.append(history.history['loss'][0])
        # self.val_los_list.append(history.history['val_loss'][0])
        # self.count_step+=1
        # self.steps_list.append(self.count_step)


        # for train_sample in train_samples:
        #     state, predef_act, reward, new_state, done = train_sample
        #     target = self.main_net.predict(state)
        #     #to zero all others
        #     #target[0]=0.0

        #     if done:
        #         target[0][predef_act] = reward
        #     else:
        #         Q = max(self.main_net.predict(new_state)[0])
        #         target[0][predef_act] = reward + Q * self.gamma
        #     #normalize reward to 0;1 as we use sigmoid which is in 0;1
        #     #target[0][predef_act] = self.normalize_reward(target[0][predef_act])
        #     self.main_net.fit(state,target,epochs = 1, verbose =0 )

    def trainTargetNet(self):
        # weights_main = self.main_net.get_weights()
        # weights_target = self.target_net.get_weights()
#        for i in range(len(weights_target)):
#            weights_target[i] = weights_main[i]*self.tau + weights_target[i]*(1-self.tau)
        self.target_net.set_weights(self.main_net.get_weights())
        #print ("target updated")

    def choseAction(self, state, len_predef_actions):
        if (self.epsilon>self.epsilon_min) and (len(self.memory)>=20000):
            self.epsilon *= self.epsilon_decay
        self.epsilon = max(self.epsilon, self.epsilon_min)
        if np.random.random() < self.epsilon:
            return random.randrange(len_predef_actions-1)
        return np.argmax(self.main_net.predict(state)[0])

    def saveNet(self,fn):
        self.main_net.save(fn)

def main():
    print("started")
    act_rest = np.array([0.0, 0.0, 0.0, 0.0])
    act_all_up = np.array([1.0, 1.0, 1.0, 1.0])
    act_all_down = np.array([-1.0, -1.0, -1.0, -1.0])
    act_both_knee_up = np.array([0.0, 1.0, 0.0, 1.0])
    act_both_knee_down = np.array([0.0, -1.0, 0.0, -1.0])
    act_both_hip_up = np.array([1.0, 0.0, 1.0, 0.0])
    act_both_hip_down = np.array([-1.0, 0.0, -1.0, 0.0])
    act_hip_1_up = np.array([1.0, 0.0, 0.0, 0.0])
    act_hip_2_up = np.array([0.0, 0.0, 1.0, 0.0])
    act_knee_1_up = np.array([0.0, 1.0, 0.0, 0.0])
    act_knee_2_up = np.array([0.0, 0.0, 0.0, 1.0])
    act_hip_1_down = np.array([-1.0, 0.0, 0.0, 0.0])
    act_hip_2_down = np.array([0.0, 0.0, -1.0, 0.0])
    act_knee_1_down = np.array([0.0, -1.0, 0.0, 0.0])
    act_knee_2_down = np.array([0.0, 0.0, 0.0, -1.0])
    act_hip_1_up_hip_2_down = np.array([1.0, 0.0, -1.0, 0.0])
    act_hip_2_up_hip_1_down = np.array([-1.0, 0.0, 1.0, 0.0])
    act_knee_1_up_knee_2_down = np.array([0.0, 1.0, 0.0, -1.0])
    act_knee_2_up_knee_1_down = np.array([0.0, -1.0, 0.0, 1.0])
    act_hip_1_up_knee_1_up = np.array([1.0, 1.0, 0.0, 0.0])
    act_hip_2_up_knee_2_up = np.array([0.0, 0.0, 1.0, 1.0])
    act_hip_1_down_knee_1_down = np.array([-1.0, -1.0, 0.0, 0.0])
    act_hip_2_down_knee_2_down = np.array([0.0, 0.0, -1.0, -1.0])
    act_hip_1_up_knee_1_down = np.array([1.0, -1.0, 0.0, 0.0])
    act_hip_2_up_knee_2_down = np.array([0.0, 0.0, 1.0, -1.0])
    act_hip_1_down_knee_1_up = np.array([-1.0, 1.0, 0.0, 0.0])
    act_hip_2_down_knee_2_up = np.array([0.0, 0.0, -1.0, 1.0])
    act_mix = np.array([1.0, -1.0, -1.0, 1.0])
    act_unmix = np.array([-1.0, 1.0, 1.0, -1.0])
    act_bi = np.array([1.0, 1.0, -1.0, -1.0])
    act_unbi = np.array([-1.0, -1.0, 1.0, 1.0])

    predef_actions = [act_rest,act_all_up,act_all_down,act_both_knee_up,act_both_knee_down,act_both_hip_up,act_both_hip_down,act_hip_1_up,act_hip_2_up,act_knee_1_up,act_knee_2_up,act_hip_1_down,act_hip_2_down,act_knee_1_down,act_knee_2_down,act_hip_1_up_hip_2_down,act_hip_2_up_hip_1_down,act_knee_1_up_knee_2_down,act_knee_2_up_knee_1_down,act_hip_1_up_knee_1_up,act_hip_2_up_knee_2_up,act_hip_1_down_knee_1_down,act_hip_2_down_knee_2_down,act_hip_1_up_knee_1_down,act_hip_2_up_knee_2_down,act_hip_1_down_knee_1_up,act_hip_2_down_knee_2_up,act_mix,act_unmix,act_bi,act_unbi]

    # env = gym.make('BipedalWalker-v2')
    env = gym.make('BipedalWalkerHardcore-v2')

    agent = mfril(env = env)

    epochs = 4000
    try_steps = 500

    record = -1000

    total_steps = 0

    scores, epochs_list = [] , []
    Qs = []
    maxQs = []
    for epoch in range(epochs):
        done = False
        cur_state = env.reset().reshape(1,24)
        reward_epoch = 0
        # for step in range(try_steps):
        while not done:
            if (epoch%100 == 0) or (epoch%100 == 1) or (epoch%100 == 2) or (epoch%100 == 3) or (epoch%100 == 4) or (epoch%100 == 5) or (epoch%100 == 6) or (epoch%100 == 7) or (epoch%100 == 8) or (epoch%100 == 9):
                env.render()

            action_index = agent.choseAction(cur_state, 30)

            new_state, reward, done, _ = env.step(predef_actions[action_index])

            # if reward == agent.min_reward_env: #death
            #     reward = agent.min_reward
            # else:              #reach the end
            #     reward = reward+agent.max_reward_emph

            #normalize reward to 0;1 as we use sigmoid which is in 0;1
            #reward = agent.normalize_reward(reward)

            #if reward<-1: reward = -1 #for tanh
            #if reward> 1: reward = 1  #for tanh
            #reward = np.clip(reward,-1,1)

            #reward = reward if not done else -20

            new_state = new_state.reshape(1,24)

            #reward = reward if not done or reward_epoch > 300 else -100

            agent.remember(cur_state, action_index, reward, new_state, done)


            Qs.append(agent.trainMainNet())

            reward_epoch+=reward
            cur_state = new_state

            # if step%5 == 0:
            #     agent.trainMainNet()

            total_steps +=1
            # if total_steps%10000 == 0:
            #      agent.trainTargetNet()

            if done:
                #agent.trainTargetNet()
                maxQs.append(np.max(Qs))
                Qs.clear()
                #reward_epoch = reward_epoch if reward_epoch > 300 else reward_epoch + 100
                #scores.append(reward_epoch)
                #epochs_list.append(epoch)

                #show score graphic
                # if (epoch%1000 == 0):
                #     pylab.plot(epochs_list, scores, 'b')
                #     pylab.xlabel('episode')
                #     pylab.ylabel('score')
                #     pylab.savefig("scores_dqn_4.png")

                # #show training and validation accuracy
                # pylab.plot( agent.steps_list, agent.acc_list, 'g')
                # pylab.plot( agent.steps_list, agent.val_acc_list, 'b')
                # pylab.xlabel('step')
                # pylab.ylabel('accuracy')
                # pylab.legend(['Train', 'Test'], loc='upper left')
                # pylab.savefig("acc_dqn.png")

                # #show training and validation loss
                # pylab.plot( agent.steps_list, agent.loss_list, 'g')
                # pylab.plot( agent.steps_list, agent.val_los_list, 'b')
                # pylab.xlabel('step')
                # pylab.ylabel('loss')
                # pylab.legend(['Train', 'Test'], loc='upper left')
                # pylab.savefig("loss_dqn.png")

                #avg_10 = np.average(scores[-min(10, len(scores)):])

                #print("episode:", epoch, "  score:", reward_epoch, "avg last 10:",avg_10)
                # if np.mean(scores[-min(10, len(scores)):]) > 490:
                #     sys.exit()



        if reward_epoch>record:
            record = reward_epoch
            print("new record {0} on epoch {1}".format(record,epoch))
            agent.trainTargetNet()
        if epoch % 1000 == 0:
            agent.saveNet("model DQN_3 train record {}".format(epoch))


        #print("epoch done {}".format(epoch))

        scores.append(reward_epoch)
        epochs_list.append(epoch)
        reward_epoch = 0
        print(epoch)



    pylab.plot(epochs_list, scores, 'b')
    if len(scores) > 100:
        mav = np.convolve(scores, np.ones((100,))/100, mode='valid')
        pylab.plot(mav, 'r', label= 'MovingAv100')
    pylab.xlabel('episode')
    pylab.ylabel('score')
    pylab.savefig("scores_dqn_avg_record_2.png")
    pylab.clf()


    pylab.plot(epochs_list, maxQs, 'b')
    if len(maxQs) > 100:
        mav = np.convolve(maxQs, np.ones((100,))/100, mode='valid')
        pylab.plot(mav, 'r', label= 'MovingAv100')
    pylab.xlabel('episode')
    pylab.ylabel('Qs')
    pylab.savefig("scores_dqn_Qs_record.png")    


if __name__ == "__main__":
    main()

