import gym
import pylab
import numpy as np
import random
from collections import deque
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam

class model_base:
    def __init__(self):
        self.action = np.array([0.0, 0.0, 0.0, 0.0])
        self.STAY_ON_ONE_LEG, self.PUT_OTHER_DOWN, self.PUSH_OFF = 1,2,3
        self.SPEED = 0.29
        self.state = self.STAY_ON_ONE_LEG
        self.moving_leg = 0
        self.support_leg = 1 - self.moving_leg
        self.SUPPORT_KNEE_ANGLE = +0.1    
        self.supporting_knee_angle = self.SUPPORT_KNEE_ANGLE

    def make_action(self, in_state, observation):
        contact0 = observation[0][8]  # is leg0 in contact with the land ? 0 - no, 1 - yes
        contact1 = observation[0][13] # is leg1 in contact with the land ? 0 - no, 1 - yes
        moving_s_base = 4 + 5*self.moving_leg   # as the index of the hip from the observation is either 4, or 9 
        support_s_base = 4 + 5*self.support_leg # as the index of the hip from the observation is either 4, or 9
        
        hip_targ  = [None,None]   # values from the range -0.8 .. +1.1 
        knee_targ = [None,None]   # values from the range -0.6 .. +0.9
        hip_todo  = [0.0, 0.0]
        knee_todo = [0.0, 0.0]

        action = np.array([0.0, 0.0, 0.0, 0.0])

        if in_state==self.STAY_ON_ONE_LEG:
            hip_targ[self.moving_leg]   =  1.1 # move the hip of the moving leg to the maximum angle
            knee_targ[self.moving_leg]  = -0.6 # move the knee of the moving leg to the minimum angle
            self.supporting_knee_angle += 0.03 # move the supporting knee in positive direction in 0.03 
            if observation[0][2] > self.SPEED: self.supporting_knee_angle += 0.03 #if velocity in x direction of the head is higher than speed -> increase support knee angle
            self.supporting_knee_angle = min(self.supporting_knee_angle, self.SUPPORT_KNEE_ANGLE )
            knee_targ[self.support_leg] = self.supporting_knee_angle

        if in_state==self.PUT_OTHER_DOWN:
            hip_targ[self.moving_leg]  = +0.1
            knee_targ[self.moving_leg] = self.SUPPORT_KNEE_ANGLE
            knee_targ[self.support_leg] = self.supporting_knee_angle
            if observation[0][moving_s_base+4]: #if there is a contact with a ground of the moving leg
                self.supporting_knee_angle = min(observation[0][moving_s_base+2], self.SUPPORT_KNEE_ANGLE ) #choose min between support knee angle and current angle
        
        if in_state==self.PUSH_OFF:
            knee_targ[self.moving_leg] = self.supporting_knee_angle
            knee_targ[self.support_leg] = +1.0
            if observation[0][support_s_base+2] > 0.88 or observation[0][2] > 1.2*self.SPEED: # if angle of supporting knee >0.88 or heads velocity in x direction is larger than 1.2*SPEDD ->  
                self.moving_leg = 1 - self.moving_leg                       # change the legs
                self.support_leg = 1 - self.moving_leg        
        
        if hip_targ[0]: hip_todo[0] = 0.9*(hip_targ[0] - observation[0][4]) - 0.25*observation[0][5]  #some_coefficient * (target_hip_angle-current_hip_angle)-some_other_coefficient*hip_curent_speed
        if hip_targ[1]: hip_todo[1] = 0.9*(hip_targ[1] - observation[0][9]) - 0.25*observation[0][10]
        if knee_targ[0]: knee_todo[0] = 4.0*(knee_targ[0] - observation[0][6])  - 0.25*observation[0][7]
        if knee_targ[1]: knee_todo[1] = 4.0*(knee_targ[1] - observation[0][11]) - 0.25*observation[0][12]

        hip_todo[0] -= 0.9*(0-observation[0][0]) - 1.5*observation[0][1] # PID to keep head strait
        hip_todo[1] -= 0.9*(0-observation[0][0]) - 1.5*observation[0][1]
        knee_todo[0] -= 15.0*observation[0][3]  # vertical speed, to damp oscillations
        knee_todo[1] -= 15.0*observation[0][3]

        action[0] = hip_todo[0]
        action[1] = knee_todo[0]
        action[2] = hip_todo[1]
        action[3] = knee_todo[1]
        action = np.clip(0.5*action, -1.0, 1.0)
        return action

class mfril:
    def __init__(self,env):
        self.env = env
        self.memory = deque(maxlen=1000000)

        self.gamma = 0.99
        self.epsilon = 1.0
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.999
        self.tau = 0.125
        self.learn_rate = 0.001

        self.batch =  64

        self.main_net = self.createNet()   #model net
        self.target_net = self.createNet() #target_net

        self.min_reward = -200
        self.max_reward_emph =  100 #emphasize reward, if reached the goal

        self.min_reward_env = -100 #from the environment code
        self.max_reward_env =  300 #from the environment code
        self.count_step = 0
        self.acc_list, self.val_acc_list, self.loss_list, self.val_los_list, self.steps_list = [] , [] , [] , [] , []

    def createNet(self):
        print("net create")
        net   = Sequential()
        state_shape  = self.env.observation_space.shape #in our case it is 24
        net.add(Dense(24, input_dim = state_shape[0], activation="relu",kernel_initializer='he_uniform'))
        net.add(Dense(72, activation="relu",kernel_initializer='he_uniform'))
        #net.add(Dense(216, activation="relu",kernel_initializer='he_uniform'))
        net.add(Dense(90, activation="relu",kernel_initializer='he_uniform'))
        net.add(Dense(3, activation="linear",kernel_initializer='he_uniform')) # as we have 3 predef states
        net.compile(loss="mse", optimizer=Adam(lr=self.learn_rate))
        return net

    def remember(self, state, act_ind, reward, new_state, done):
        self.memory.append([state, act_ind, reward, new_state, done])

    def normalize_reward(self, in_reward):
        #normalizing reward to [0,1]
        #where 0 = self.min_reward
        #      +1 = self.max_reward_env+self.max_reward_emph
        return (in_reward-self.min_reward)/(self.max_reward_env+self.max_reward_emph-self.min_reward)


    def trainMainNet(self):
        if (len(self.memory)<10000):
            return 0
        train_samples = random.sample(self.memory, self.batch)

        initial_states  = np.zeros((self.batch,(self.env.observation_space.shape)[0]))
        update_target = np.zeros((self.batch,(self.env.observation_space.shape)[0]))
        model_states, rewards, dones = [], [], []

        for i in range(self.batch):
            initial_states[i] = train_samples[i][0]     #states
            model_states.append(train_samples[i][1])  #action_indexes
            rewards.append(train_samples[i][2])         #rewards
            update_target[i] = train_samples[i][3]      #new_states
            dones.append(train_samples[i][4])           #dones

        predict_rewards = self.main_net.predict(initial_states)
        predict_Qs = self.target_net.predict(update_target)

        for i in range(self.batch):
            if dones[i]:
                predict_rewards[i][model_states[i]]=rewards[i]
            else:
                predict_rewards[i][model_states[i]]=rewards[i]+self.gamma*(np.amax(predict_Qs[i]))

        self.main_net.fit(initial_states,predict_rewards, batch_size=self.batch, epochs=1, verbose=0)

        return np.max(predict_Qs)

    def trainTargetNet(self):
        self.target_net.set_weights(self.main_net.get_weights())

    def choseAction(self, state, model_state, len_predef_actions, mb):
        if (self.epsilon>self.epsilon_min) and (len(self.memory)>=20000):
            self.epsilon *= self.epsilon_decay
        self.epsilon = max(self.epsilon, self.epsilon_min)
        if np.random.random() < self.epsilon:
            model_state_out = random.randrange(len_predef_actions-1)
            action_out =  mb.make_action(model_state_out, state)
            return model_state_out,action_out
            
        model_state_out = np.argmax(self.main_net.predict(state)[0])
        action_out =  mb.make_action(model_state_out, state)
        return model_state_out,action_out
    

    def saveNet(self,fn):
        self.main_net.save(fn)

def main():
    print("started")
    STAY_ON_ONE_LEG, PUT_OTHER_DOWN, PUSH_OFF = 1,2,3

    env = gym.make('BipedalWalkerHardcore-v2')

    agent = mfril(env = env)

    epochs = 4000
    try_steps = 500

    record = -1000

    total_steps = 0

    scores, epochs_list = [] , []
    Qs = []
    maxQs = []
    for epoch in range(epochs):
        comp_model = model_base()
        action = [0.0, 0.0, 0.0, 0.0]
        model_state = STAY_ON_ONE_LEG
        done = False 

        cur_state = env.reset().reshape(1,24)
        reward_epoch = 0
        # for step in range(try_steps):
        while not done:
            if (epoch%100 == 0) or (epoch%100 == 1) or (epoch%100 == 2) or (epoch%100 == 3) or (epoch%100 == 4) or (epoch%100 == 5) or (epoch%100 == 6) or (epoch%100 == 7) or (epoch%100 == 8) or (epoch%100 == 9):
                env.render()
            #env.render()    

            model_state,action = agent.choseAction(cur_state,model_state, 3,comp_model)

            new_state, reward, done, _ = env.step(action)

            new_state = new_state.reshape(1,24)

            agent.remember(cur_state, model_state, reward, new_state, done)

            Qs.append(agent.trainMainNet())

            reward_epoch+=reward
            cur_state = new_state

            total_steps +=1

            if done:
                agent.trainTargetNet()
                maxQs.append(np.max(Qs))
                Qs.clear()
        

        if reward_epoch>record:
            record = reward_epoch
            print("new record {0} on epoch {1}".format(record,epoch))
            #agent.trainTargetNet()
        if epoch % 1000 == 0:
            agent.saveNet("model DQN_3 model based {}".format(epoch))


        scores.append(reward_epoch)
        epochs_list.append(epoch)
        reward_epoch = 0
        print(epoch)



    pylab.plot(epochs_list, scores, 'b')
    if len(scores) > 100:
        mav = np.convolve(scores, np.ones((100,))/100, mode='valid')
        pylab.plot(mav, 'r', label= 'MovingAv100')
    pylab.xlabel('episode')
    pylab.ylabel('score')
    pylab.savefig("scores_dqn_avg_model.png")
    pylab.clf()


    pylab.plot(epochs_list, maxQs, 'b')
    if len(maxQs) > 100:
        mav = np.convolve(maxQs, np.ones((100,))/100, mode='valid')
        pylab.plot(mav, 'r', label= 'MovingAv100')
    pylab.xlabel('episode')
    pylab.ylabel('Qs')
    pylab.savefig("scores_dqn_Qs_model.png")    


if __name__ == "__main__":
    main()

