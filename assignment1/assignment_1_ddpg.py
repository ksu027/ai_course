#based on the code from https://github.com/yanpanlau/DDPG-Keras-Torcs

import gym
import pylab
import numpy as np
import keras
from keras.models import model_from_json, load_model
from keras.models import Sequential
from keras.layers import Dense, Flatten, Input, merge, Lambda, Activation, Dropout, concatenate, add
from keras.models import Model
import keras.optimizers as Kopt
import keras.backend as K
import tensorflow as tf
from keras.utils import plot_model
import matplotlib.pyplot as plt

import math, random
from collections import deque
import TempConfig


HIDDEN1_UNITS = 120
HIDDEN2_UNITS = 140
# Huber loss fucntion
# y_true - true value
# y_pred - predicted value
HUBER_LOSS_DELTA = 1 
def huber_loss(y_true, y_pred):
    err = y_true - y_pred

    cond = K.abs(err) <= HUBER_LOSS_DELTA
    if cond == True:
        loss = 0.5 * K.square(err)       
    else:
        loss = 0.5 * HUBER_LOSS_DELTA**2 + HUBER_LOSS_DELTA * (K.abs(err) - HUBER_LOSS_DELTA)

    return K.mean(loss)

# critic net. Q-network
class CriticNetwork(object):
    def __init__(self, sess, state_size, action_size, BATCH_SIZE, TAU, LEARNING_RATE):
        self.sess = sess
        self.BATCH_SIZE = BATCH_SIZE
        self.TAU = TAU
        self.LEARNING_RATE = LEARNING_RATE
        self.action_size = action_size
        
        K.set_session(sess) #session for TensorFlow

        #model creation
        self.model, self.action, self.state = self.create_critic_network(state_size, action_size)  
        self.target_model, self.target_action, self.target_state = self.create_critic_network(state_size, action_size)  
        self.action_grads = tf.gradients(self.model.output, self.action)  #GRADIENTS for policy update
        self.sess.run(tf.initialize_all_variables())
           

    def gradients(self, states, actions):
        return self.sess.run(self.action_grads, feed_dict={
            self.state: states,
            self.action: actions
        })[0]

    def target_train(self):
        critic_weights = self.model.get_weights()
        critic_target_weights = self.target_model.get_weights()
        for i in range(len(critic_weights)):
            critic_target_weights[i] = self.TAU * critic_weights[i] + (1 - self.TAU)* critic_target_weights[i]
        self.target_model.set_weights(critic_target_weights)

    def create_critic_network(self, state_size,action_dim):
        print("Create a critic model")
        S = Input(shape=[state_size])  
        A = Input(shape=[action_dim],name='action2')   
        w1 = Dense(HIDDEN1_UNITS, activation='relu')(S)
        a1 = Dense(HIDDEN2_UNITS, activation='relu')(A) 
        h1 = Dense(HIDDEN2_UNITS, activation='relu')(w1)
        #h2 = merge([h1,a1],mode='sum')    
        h2 = add([h1,a1])
        h3 = Dense(HIDDEN2_UNITS, activation='relu')(h2)
        V = Dense(action_dim,activation='linear')(h3)   
        model = Model(input=[S,A],output=V)
        #adam = Kopt.Adam(lr=self.LEARNING_RATE)
        self.opt = Kopt.RMSprop(lr=self.LEARNING_RATE)
        
        model.compile(loss=huber_loss, optimizer=self.opt) #'mse'
        
        #plot_model(model, to_file='DDPG_Critic_model.png', show_shapes = True)
        return model, A, S


#actor net  deterministic policy
class ActorNetwork(object):
    def __init__(self, sess, state_size, action_size, BATCH_SIZE, TAU, LEARNING_RATE):
        self.sess = sess
        self.BATCH_SIZE = BATCH_SIZE
        self.TAU = TAU
        self.LEARNING_RATE = LEARNING_RATE

        K.set_session(sess)

        #Now create the model
        self.model , self.weights, self.state = self.create_actor_network(state_size)   
        self.target_model, self.target_weights, self.target_state = self.create_actor_network(state_size) 
        self.action_gradient = tf.placeholder(tf.float32,[None, action_size])
        self.params_grad = tf.gradients(self.model.output, self.weights, -self.action_gradient)
        grads = zip(self.params_grad, self.weights)
        self.optimize = tf.train.AdamOptimizer(LEARNING_RATE).apply_gradients(grads)
        self.sess.run(tf.initialize_all_variables())

    def train(self, states, action_grads):
        self.sess.run(self.optimize, feed_dict={
            self.state: states,
            self.action_gradient: action_grads
        })

    def target_train(self):
        actor_weights = self.model.get_weights()
        actor_target_weights = self.target_model.get_weights()
        for i in range(len(actor_weights)):
            actor_target_weights[i] = self.TAU * actor_weights[i] + (1 - self.TAU)* actor_target_weights[i]
        self.target_model.set_weights(actor_target_weights)

    def create_actor_network(self, state_size):
        print("Create an actor model")
        S = Input(shape=[state_size])   
        h0 = Dense(HIDDEN1_UNITS, activation='tanh')(S)
        h1 = Dense(HIDDEN2_UNITS, activation='tanh')(h0)
        h1 = Dense(HIDDEN2_UNITS, activation='tanh')(h1)
        
        init1 = keras.initializers.RandomNormal(mean=0, stddev = 1/np.sqrt(state_size), seed=20)
        act1 = Dense(1,activation='tanh',init=init1)(h1)
        act2 = Dense(1,activation='tanh',init=init1)(h1)
        act3 = Dense(1,activation='tanh',init=init1)(h1)
        act4 = Dense(1,activation='tanh',init=init1)(h1)

        
        #V = merge([act1, act2],mode='concat')   
        V = concatenate([act1, act2, act3, act4])       
        model = Model(input=S,output=V)
        
        #plot_model(model, to_file='DDPG_Actor_model.png', show_shapes = True)
        return model, model.trainable_weights, S

# gaussian noise 
class OrnsteinUhlenbeckActionNoise:

	def __init__(self, action_dim, mu = 0, theta = 0.15, sigma = 0.2):
		self.action_dim = action_dim
		self.mu = mu
		self.theta = theta
		self.sigma = sigma
		self.X = np.ones(self.action_dim) * self.mu

	def reset(self):
		self.X = np.ones(self.action_dim) * self.mu

	def sample(self):
		dx = self.theta * (self.mu - self.X)
		dx = dx + self.sigma * np.random.randn(len(self.X))
		self.X = self.X + dx
		return self.X        


class ReplayBuffer(object):

    def __init__(self, buffer_size):
        self.buffer_size = buffer_size
        self.num_experiences = 0
        self.buffer = deque()

    def getBatch(self, batch_size):
        # Randomly sample batch_size examples
        if self.num_experiences < batch_size:
            return random.sample(self.buffer, self.num_experiences)
        else:
            return random.sample(self.buffer, batch_size)

    def size(self):
        return self.buffer_size

    def add(self, state, action, reward, new_state, done):
        experience = (state, action, reward, new_state, done)
        if self.num_experiences < self.buffer_size:
            self.buffer.append(experience)
            self.num_experiences += 1
        else:
            self.buffer.popleft()
            self.buffer.append(experience)

    def count(self):
        # if buffer is full, return buffer size
        # otherwise, return experience counter
        return self.num_experiences

    def erase(self):
        self.buffer = deque()
        self.num_experiences = 0


LoadWeithsAndTrain = False
LoadWeithsAndTest = False

BUFFER_SIZE = 150000
BATCH_SIZE = 99
STATE_NORM = (10, 1) #(1/gain, ofs)
STATE_NORM_single = [(10, 0), (1, 0), (2, 0), (5, 0.9), (3, 1)] 
REW_MIN = -20

GAMMA = 0.99
TAU = 0.001     #Target Network HyperParameters

LRA = 0.0001    #Learning rate for Actor
LRC = 0.001     #Lerning rate for Critic

episode_count = 4000
max_steps = 500

MAX_EPSILON = 1
MIN_EPSILON = 0.1

EXPLORATION_STOP = int(max_steps)        # at this step epsilon will be MIN_EPSILON
LAMBDA = - math.log(0.01) / EXPLORATION_STOP  # speed of decay fn of episodes of learning agent


def plot_logs(f, ax1, reward, act1, act2,act3, act4, loss, eps):
    
    f.set_figwidth(10)
    f.set_figheight(10)

    ax1[0].set_title('Reward')
    #ax1.set_xlim([0,50])
    ax1[0].plot(reward, label= 'Reward')
    if len(reward) > 100:
        mav = np.convolve(reward, np.ones((100,))/100, mode='valid')
        ax1[0].plot(mav, label= 'MovingAv100')
    ax1[0].legend(loc='upper left')
   
    ax1[1].plot(act1, label= 'action1')  
    ax1[1].plot(act2, label= 'action2')
    ax1[1].plot(act3, label= 'action3')  
    ax1[1].plot(act4, label= 'action4')
    ax1[1].legend(loc='upper right')
    ax1[3].plot(loss, label= 'Training loss')    
    ax1[3].legend(loc='upper right')

    ax1[2].plot(eps, label= 'Epsilon')    
    ax1[2].legend(loc='upper right')
    
    f.savefig('RL_DDPG_Plant_2.png')
    ax[0].cla(); ax[1].cla(); ax[2].cla(); ax[3].cla()
    #ax1.cla()
    plt.close(f)
   
def normalize_s(s, scale=STATE_NORM, single=False):
    if not single:
        s = scale[1] + s/scale[0]
    else:
        for i in range(len(scale)):
            s[i] = scale[i][1] + s[i]/scale[i][0]
            
    return (s)

def Test_Agent(actor, env, render=False):
    #Agent for policy validation after training            
    total_reward = 0; Act1 = []; Act2 = []
    s_t = env.reset()
    s_t = normalize_s(s_t)
    done =False
    while not done:
        if render:
            env.render()
        a_t = actor.model.predict(s_t.reshape(1, s_t.shape[0]))
        a_t[0][0] = a_t[0][0] * A_MAX[0][0]  + A_MAX[1][0]
        a_t[0][1] = a_t[0][1] * A_MAX[0][1]  + A_MAX[1][1]
        #Correct scaling!
        s_t, r_t, done, info = env.step(a_t[0])
        s_t = normalize_s(s_t)
        total_reward += r_t
        Act1.append(a_t[0][0])
        Act2.append(a_t[0][1])
    
    print("Episode:" + str(done_ctr) +": Reward " + str(total_reward)) 
    
    return (total_reward, Act1, Act2)    

if __name__ == "__main__":
    
    #np.random.seed(17)

    reward = 0
    done = False
    step = 0
    epsilon = []

    sess = tf.Session()
    K.set_session(sess)
    
    f, ax = plt.subplots(4,1)
    buff = ReplayBuffer(BUFFER_SIZE)    #Create replay buffer
    noise1 = OrnsteinUhlenbeckActionNoise(1, mu = 0, theta = 0.15, sigma = 0.75)
    noise2 = OrnsteinUhlenbeckActionNoise(1, mu = 0, theta = 0.15, sigma = 0.75)
    noise3 = OrnsteinUhlenbeckActionNoise(1, mu = 0, theta = 0.15, sigma = 0.75)
    noise4 = OrnsteinUhlenbeckActionNoise(1, mu = 0, theta = 0.15, sigma = 0.75)   
    
    # Get environment
    env = gym.make('BipedalWalkerHardcore-v2')
    state_dim = (env.observation_space.shape)[0] #24
    action_dim = 4#(env.action_space.shape)[0]     #4
    A_MAX = [[-1,1],[-1,1],[-1,1],[-1,1]] 

    
    actor = ActorNetwork(sess, state_dim, action_dim, BATCH_SIZE, TAU, LRA)
    critic = CriticNetwork(sess, state_dim, action_dim, BATCH_SIZE, TAU, LRC)
    
    # if LoadWeithsAndTrain == True: 
    #     try:
    #         ram = TempConfig.load_pickle("Memory_Rnd")
    #         print('Random memory loaded')
    #     except:
    #         print('Error while loading random memory')
                 
#        print("Now we load the weight")
#        try:
#            actor.model.load_weights(TempConfig.ModelsPath+"Plant_DDPG_actor_model.h5")
#            critic.model.load_weights(TempConfig.ModelsPath+"Plant_DDPG_critic_model.h5")
#            actor.target_model.load_weights(TempConfig.ModelsPath+"Plant_DDPG_actor_model.h5")
#            critic.target_model.load_weights(TempConfig.ModelsPath+"Plant_DDPG_critic_model.h5")
#            print("Weight load successfully")
#        except:
#            print("Cannot find the weight")

    print("Experiment Start.")
    R = []; step_ctr = 0
    ListQs = []
    epochs_list = []
    ListQs_max = []
    steps = []
    total_steps = 0
    try:  
        if LoadWeithsAndTest == False:
            #Train policiy
            for i in range(episode_count):
                epochs_list.append(i)
                print("Episode " + str(i) + "/"+ str(episode_count) +" Replay Buffer " + str(buff.count()) + "/"+ str(BUFFER_SIZE) )
        
                #ob = env.reset()
                #s_t = normalize_s(ob) #for now without normalization
                s_t = cur_state = env.reset().reshape(1,24)
                
                Act1=[]; Act2=[]; Act3=[]; Act4=[]; L=[]
                total_reward = 0
                to_continue = True
                #for j in range(max_steps): 
                while to_continue :     
                    if i % 1000 == 0 or i % 1000 == 1 or i % 1000 == 2 or i % 1000 == 3 or i % 1000 == 4 :     
                        env.render()

                    loss = 0 
                    step_ctr +=1 #global steps counting
                    epsilon.append(MIN_EPSILON + (MAX_EPSILON - MIN_EPSILON) * math.exp(-LAMBDA * step_ctr))
                    a_t = np.zeros([1,action_dim])
                    a_t_ = np.zeros([1,action_dim])
                    noise_t = np.zeros([1,action_dim])
                    
                    #a_t_original = actor.model.predict(s_t.reshape(1, s_t.shape[0]))
                    a_t_original = actor.model.predict(s_t)
                    
                    if epsilon[-1] > MIN_EPSILON + 0.02:
                        noise_t[0][0] =  max(epsilon[-1], 0) * noise1.sample() #OU.function(a_t_original[0][0],  0.5 , 0.80, 0.5) #x,mu,teta,sigma
                        noise_t[0][1] =  max(epsilon[-1], 0) * noise2.sample() #OU.function(a_t_original[0][1],  0.5 , 0.80, 0.5)
                        noise_t[0][2] =  max(epsilon[-1], 0) * noise3.sample() #OU.function(a_t_original[0][0],  0.5 , 0.80, 0.5) #x,mu,teta,sigma
                        noise_t[0][3] =  max(epsilon[-1], 0) * noise4.sample() #OU.function(a_t_original[0][1],  0.5 , 0.80, 0.5)
                    else:
                        if random.random() < MIN_EPSILON:
                            noise_t[0][0] =  0.5 * noise1.sample() 
                            noise_t[0][1] =  0.5 * noise2.sample()
                            noise_t[0][2] =  0.5 * noise3.sample() 
                            noise_t[0][3] =  0.5 * noise4.sample()                            
                        else:                                                                                                                                                                                                                                               
                            noise_t[0][0] = 0.0 * noise1.sample()
                            noise_t[0][1] = 0.0 * noise1.sample()
                            noise_t[0][2] = 0.0 * noise1.sample()
                            noise_t[0][3] = 0.0 * noise1.sample()                            
                        
                    # Scale  actions 
                    a_t_[0][0] = np.clip( a_t_original[0][0] + noise_t[0][0], -1, 1 ) 
                    a_t_[0][1] = np.clip( a_t_original[0][1] + noise_t[0][1], -1, 1 )
                    a_t_[0][2] = np.clip( a_t_original[0][2] + noise_t[0][2], -1, 1 ) 
                    a_t_[0][3] = np.clip( a_t_original[0][3] + noise_t[0][3], -1, 1 )

                    a_t[0][0] = a_t_[0][0]# * (-1.0)  + (1.0)
                    a_t[0][1] = a_t_[0][1]# * (-1.0)  + (1.0)
                    a_t[0][2] = a_t_[0][2]# * (-1.0)  + (1.0)
                    a_t[0][3] = a_t_[0][3]# * (-1.0)  + (1.0)                    
        
                    s_t1, r_t, done, info = env.step(a_t[0])                                                                                                                                                                    
                    #r_t = np.clip(r_t, REW_MIN, 1) #-9, 1
                    #s_t1 = normalize_s(s_t1) without normalization for now
                    s_t1 = s_t1.reshape(1,24)
                    buff.add(s_t[0], a_t_[0], r_t, s_t1[0], done)      #Add replay buffer
                    
                    #Do the batch update
                    batch = buff.getBatch(BATCH_SIZE)
                    states = np.asarray([e[0] for e in batch])
                    actions = np.asarray([e[1] for e in batch])
                    rewards = np.asarray([e[2] for e in batch])
                    new_states = np.asarray([e[3] for e in batch])
                    dones = np.asarray([e[4] for e in batch])
                    y_t = np.asarray([e[1] for e in batch])
        
                    #print (states.shape)
                    target_q_values = critic.target_model.predict([new_states, actor.target_model.predict(new_states)])  
                   
                    for k in range(len(batch)):
                        if dones[k]:
                            y_t[k] = rewards[k]
                        else:
                            y_t[k] = rewards[k] + GAMMA*target_q_values[k]

                
                    #list of max predicted Qs
                    total_steps+=1
                    ListQs.append(np.max(target_q_values))
                    steps.append(total_steps)
                    
               
                    
                    loss += critic.model.train_on_batch([states,actions], y_t)
                    a_for_grad = actor.model.predict(states)
                    grads = critic.gradients(states, a_for_grad)
                    actor.train(states, grads)
                    actor.target_train()
                    critic.target_train()
        
                    total_reward += r_t
                    s_t = s_t1
                
                    #print("Episode", i, "Step", step, "Action", a_t[0])#, "Reward", r_t, "Loss", loss)
                    
                    Act1.append(a_t[0][0])
                    Act2.append(a_t[0][1])
                    Act3.append(a_t[0][2])
                    Act4.append(a_t[0][3])
                    L.append(loss)
                    
                    step += 1
                    if done:
                        ListQs_max.append(np.max(ListQs))
                        ListQs.clear()
                        to_continue = False
#                        plot_logs(f, ax, R, Act1, Act2, Act3, Act4, L, epsilon)
                        #env.render()   
                        if LoadWeithsAndTest == False:
                            TempConfig.dump_pickle(buff, 'Memory_Rnd')
                        
                        if epsilon[-1]<0.2:
                            if total_reward > max(R) :
                                print('Best models saved')
                                TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDPG_actor_model_best_2.h5", actor)
                                TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDQG_critic_model_best_2.h5", critic)
                                
                            if i % 50 == 0:
                                print('Checkpoint model saved at episode:', i)
                                TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDPG_actor_model_cp_2_"+str(i)+".h5", actor)
                                TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDPG_critic_model_cp_2_"+str(i)+".h5", critic)                            
                        break
        
                R.append(total_reward)
                
                print("TOTAL REWARD @ " + str(i) +"-th Episode  :" + str(total_reward))
                print("Total Step: " + str(step))
                print("")
            
            TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDPG_actor_model_final_2.h5", actor)
            TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDPG_critic_model_final_2.h5", critic)

            plot_logs(f, ax, R, Act1, Act2, Act3, Act4, L, epsilon)

            pylab.plot(epochs_list, ListQs_max, 'b')
            if len(ListQs) > 100:
                mav = np.convolve(ListQs_max, np.ones((100,))/100, mode='valid')
                pylab.plot(mav, 'r', label= 'MovingAv100')
            pylab.xlabel('episode')
            pylab.ylabel('Q')
            pylab.savefig("predicted_Qs.png")
            
            env.close()  

            print("Finish.")
            
        #else:
            # Test policiy
            # import glob
            # scores = {}
            
            # allWeights_actor = sorted(glob.glob(TempConfig.ModelsPath + "/Plant_DDPG_actor_model_cp*.h5"))
            # allWeights_critic = sorted(glob.glob(TempConfig.ModelsPath + "/Plant_DDPG_critic_model_cp*.h5"))
            
            # print('Load agent and play')
            
            # for i in range(len(allWeights_actor)):
            #     actor.model.load_weights(allWeights_actor[i])
            #     critic.model.load_weights(allWeights_critic[i])
            #     #actor.model.load_weights(TempConfig.ModelsPath+"Plant_DDPG_actor_model.h5")
            #     #critic.model.load_weights(TempConfig.ModelsPath+"Plant_DDPG_critic_model.h5")
                
            #     done_ctr = 0; 
            #     L=[]
            #     while done_ctr < 1 :
            #         total_reward, Act1, Act2 = Test_Agent(actor, env)
            #         R.append(total_reward) 
            #         scores[allWeights_actor[i]]=total_reward
            #         if total_reward >= max(scores.values()):
            #             best_act = allWeights_actor[i]
            #             best_crit = allWeights_critic[i]
    
            #         #plot_logs(f, ax, R, Act1, Act2, L, epsilon)   
            #         done_ctr += 1
                    
            # print('**** Best score:', max(scores.values()), ' at cp:', best_act, '****')
            
            # #render very best
            # actor.model.load_weights(best_act)
            # critic.model.load_weights(best_crit)
            # total_reward, Act1, Act2 = Test_Agent(actor, env, render=True)
            # plot_logs(f, ax, R, Act1, Act2, L, epsilon)   
                
            # env.close()            
                  
    except KeyboardInterrupt:
        print('User interrupt')
        env.close()
        plot_logs(f, ax, R, Act1, Act2, L, epsilon)  
        if LoadWeithsAndTest == False:
             print('Save model: Y or N?')
             save = input()
             if save.lower() == 'y':
                 TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDPG_actor_model_2.h5", actor)
                 TempConfig.save_DDQL(TempConfig.ModelsPath, "Plant_DDPG_critic_model_2.h5", critic)
             else:
                print('Model discarded')