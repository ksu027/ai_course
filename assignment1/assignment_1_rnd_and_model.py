#from the example

import gym
import numpy as np

import pylab

import random
from collections import deque
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam

#env = gym.make('BipedalWalker-v2')
env = gym.make('BipedalWalkerHardcore-v2')

reward_epoch = 0
scores = []
epochs_list = []

for i_episode in range(4000):
    observation = env.reset()

    reward_sum = 0
    action = np.array([0.0, 0.0, 0.0, 0.0])
    STAY_ON_ONE_LEG, PUT_OTHER_DOWN, PUSH_OFF = 1,2,3
    SPEED = 0.29
    state = STAY_ON_ONE_LEG
    moving_leg = 0
    support_leg = 1 - moving_leg
    SUPPORT_KNEE_ANGLE = +0.1    
    supporting_knee_angle = SUPPORT_KNEE_ANGLE
    done = False 

    while not done:

        observation, reward, done, info = env.step(env.action_space.sample())
        #env.render()
        reward_epoch += reward

        # #make a step
        # observation, reward, done, info = env.step(action)
        # reward_sum += reward   
        # reward_epoch += reward     
        # #env.render()
        # print(observation[0])
        
        # contact0 = observation[8]  # is leg0 in contact with the land ? 0 - no, 1 - yes
        # contact1 = observation[13] # is leg1 in contact with the land ? 0 - no, 1 - yes
        # moving_s_base = 4 + 5*moving_leg   # as the index of the hip from the observation is either 4, or 9 
        # support_s_base = 4 + 5*support_leg # as the index of the hip from the observation is either 4, or 9
        
        # hip_targ  = [None,None]   # values from the range -0.8 .. +1.1 
        # knee_targ = [None,None]   # values from the range -0.6 .. +0.9
        # hip_todo  = [0.0, 0.0]
        # knee_todo = [0.0, 0.0]

        # if state==STAY_ON_ONE_LEG:
        #     hip_targ[moving_leg]   =  1.1 # move the hip of the moving leg to the maximum angle
        #     knee_targ[moving_leg]  = -0.6 # move the knee of the moving leg to the minimum angle
        #     supporting_knee_angle += 0.03 # move the supporting knee in positive direction in 0.03 
        #     if observation[2] > SPEED: supporting_knee_angle += 0.03 #if velocity in x direction of the head is higher than speed -> increase support knee angle
        #     supporting_knee_angle = min( supporting_knee_angle, SUPPORT_KNEE_ANGLE )
        #     knee_targ[support_leg] = supporting_knee_angle
        #     if observation[support_s_base+0] < 0.10: # supporting leg is behind - > it is time to put the moving leg down
        #         state = PUT_OTHER_DOWN

        # if state==PUT_OTHER_DOWN:
        #     hip_targ[moving_leg]  = +0.1
        #     knee_targ[moving_leg] = SUPPORT_KNEE_ANGLE
        #     knee_targ[support_leg] = supporting_knee_angle
        #     if observation[moving_s_base+4]: #if there is a contact with a ground of the moving leg
        #         state = PUSH_OFF
        #         supporting_knee_angle = min( observation[moving_s_base+2], SUPPORT_KNEE_ANGLE ) #choose min between support knee angle and current angle
        
        # if state==PUSH_OFF:
        #     knee_targ[moving_leg] = supporting_knee_angle
        #     knee_targ[support_leg] = +1.0
        #     if observation[support_s_base+2] > 0.88 or observation[2] > 1.2*SPEED: # if angle of supporting knee >0.88 or heads velocity in x direction is larger than 1.2*SPEDD ->  
        #         state = STAY_ON_ONE_LEG                           # stay on one leg 
        #         moving_leg = 1 - moving_leg                       # change the legs
        #         support_leg = 1 - moving_leg        
        
        # if hip_targ[0]: hip_todo[0] = 0.9*(hip_targ[0] - observation[4]) - 0.25*observation[5]  #some_coefficient * (target_hip_angle-current_hip_angle)-some_other_coefficient*hip_curent_speed
        # if hip_targ[1]: hip_todo[1] = 0.9*(hip_targ[1] - observation[9]) - 0.25*observation[10]
        # if knee_targ[0]: knee_todo[0] = 4.0*(knee_targ[0] - observation[6])  - 0.25*observation[7]
        # if knee_targ[1]: knee_todo[1] = 4.0*(knee_targ[1] - observation[11]) - 0.25*observation[12]

        # hip_todo[0] -= 0.9*(0-observation[0]) - 1.5*observation[1] # PID to keep head strait
        # hip_todo[1] -= 0.9*(0-observation[0]) - 1.5*observation[1]
        # knee_todo[0] -= 15.0*observation[3]  # vertical speed, to damp oscillations
        # knee_todo[1] -= 15.0*observation[3]

        # action[0] = hip_todo[0]
        # action[1] = knee_todo[0]
        # action[2] = hip_todo[1]
        # action[3] = knee_todo[1]
        # action = np.clip(0.5*action, -1.0, 1.0)

    scores.append(reward_epoch)
    epochs_list.append(i_episode)
    reward_epoch = 0



pylab.plot(epochs_list, scores, 'b')
if len(scores) > 100:
    mav = np.convolve(scores, np.ones((100,))/100, mode='valid')
    pylab.plot(mav, 'r', label= 'MovingAv100')

pylab.xlabel('episode')
pylab.ylabel('score')
pylab.savefig("scores_random_avg.png")

# print (epochs_list)
# print (" ")
# print (scores)

env.close()