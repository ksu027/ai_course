import plotting
import itertools 
import matplotlib.pyplot as plt
import matplotlib.style 
import numpy as np 
import pandas as pd 

from collections import defaultdict 
from operator import itemgetter

# first create model (or environment)

class env(object):
    def __init__(self, boat_capacity, person_weight, luggage1, luggage2 ):
        self.boat_capacity = boat_capacity
        self.person_weight = person_weight
        self.luggage1      = luggage1
        self.luggage2      = luggage2

    def step(self, in_luggage1, in_luggage2):
        for lug_piece in in_luggage1:
            if self.luggage1.count(lug_piece)>0:
                self.luggage1.remove(lug_piece)
        for lug_piece in in_luggage2:
            if self.luggage2.count(lug_piece)>0:
                self.luggage2.remove(lug_piece)
        sum_lug1 = sum(in_luggage1)
        sum_lug2 = sum(in_luggage2)

        penalty1=penalty2=0
        if (sum_lug1+sum_lug2>(self.boat_capacity-self.person_weight*3)):
            # if the boat sinks - everyone penilized by -1000
            r_boat = r_p1 = r_p2 = -1000
        else:
            # otherwise
            # passengers get +10 for each kg, -100 if they have some luggage left
            penalty1 = 0 if sum(self.luggage1) == 0 else -100
            r_p1 = sum_lug1*10 + penalty1
            penalty2 = 0 if sum(self.luggage2) == 0 else -100
            r_p2 = sum_lug2*10 + penalty2
            # captain +100 for each passenger +10 for each kg
            r_boat = sum_lug1*10 + sum_lug2*10 + 100*2 + (-1)*penalty1 +(-1)*penalty2
        
        done = True if (penalty1 == 0 or penalty2 == 0) else False
        return r_boat,r_p1,r_p2,[self.luggage1.copy(),self.luggage2.copy()],done

def get_iteratorQ (in_state,in_Q):
    # find iterator when searching state in the Q
    it =0
    for state,l_m_values in in_Q:
        # that all elements in state are the same as in input state, but may be in different order
        if (all(elem in state for elem in in_state) and (all(elem in in_state for elem in state)) and len(in_state)==len(state)):
            return it
        it+=1


#compute probabilities for the action space with greedy policy
def epsilonGreedy(Q, epsilon, in_state): 
    #search in Q-matrix the current state
    for state,l_m_values in Q:
        # that all elements in state are the same as in input state, but may be in different order
        if (all(elem in state for elem in in_state) and all(elem in in_state for elem in state) and len(state)==len(in_state)):
            #spread epsilon for all possible actions
            action_probabilities = []
            for legal_move,val in l_m_values:
                action_probabilities.append((legal_move,epsilon / len(l_m_values)))
            #np.ones (len(l_m_values), dtype = float) * epsilon / len(l_m_values) 
            #look for the best action for the givven state in Q table        
            #best_action = np.argmax(l_m_values) 
            # action_probabilities[best_action] += (1.0 - epsilon)
            # add some randomness here
            # was:
            best_action = max(l_m_values,key=itemgetter(1))[0]
            # now:
            # best_action_val = max(l_m_values,key=itemgetter(1))[1]
            # list_of_best_ind = []
            # inde = 0
            # for legal_move_in,val_in in l_m_values:
            #     if val_in==best_action_val:
            #         list_of_best_ind.append(inde)
            #     inde+=1
            # best_action = l_m_values[np.random.choice(list_of_best_ind)][0] 

            legal_actions = []
            legal_probabilities = []
            one_time = True
            for act,probability in action_probabilities:
                legal_actions.append(act)
                if (all(elem in act for elem in best_action) and all(elem in best_action for elem in act) and (len(best_action)==len(act)) and one_time):
                    probability+= (1.0 - epsilon)
                    one_time = False
                legal_probabilities.append(probability)    
            return legal_actions,legal_probabilities 

def getLegalMoves(in_actions):
    # defines a list of possible states
    # each state has list of possible moves
    # combination of possible states is inverse of combination of possible actions. in next form:
    # [state_1,[[legal_move_1],[legal_move_2],...,[legal_move_n]]]
    # ...
    # [state_m,[[legal_move_1],[legal_move_2],...,[legal_move_n]]]
    out_moves = []
    for state in in_actions:
        legal_moves = []
        if (state != []) :
            for new_action in in_actions:
                #check that action consists of elements of the state only, has the size smaller or equal to the state's size, has the same amount or less number of each element as in state. 
                if (any(elem in state for elem in new_action)):
                    if (all(elem in state for elem in new_action) and all(state.count(elem)>=new_action.count(elem) for elem in new_action)):
                        if (new_action not in legal_moves) and (len(new_action)<=len(state)):
                            legal_moves.append(new_action)
        legal_moves.append([]) #add empty action to all states
        out_moves.append((state,legal_moves))
    return out_moves    

def initQ(in_legal_moves):
    # initializes Q matrix 
    # fills in for each state's possible action q-value with zero. In next form:
    # (state_1,[(legal_move_1,q_value_1),(legal_move_2,q_value_2),...,(legal_move_n,q_value_n)])
    # ...
    # (state_m,[[legal_move_1,q_value_1],[legal_move_2,q_value_2],...,[legal_move_n,q_value_n]])
    # list of tuples (state,list of tuplesc(legal_move,value))
    out_q=[]
    for state,moves in in_legal_moves:
        move_values = []
        for move in moves:
            move_values.append((move,0))
        out_q.append((state,move_values))
    return out_q

   

if __name__ == "__main__":
    boat_capacity = 500
    person_weight = 100
    luggage1      = [20,30,80,100] #available luggage of passenger 1
    luggage2      = [30,50,50,90]  #available luggage of passenger 2
    #possible actions of passenger 1 and passenger 2. hardcoded for now
    #Combinations for pas1 = 16
    #Combinations for pas2 = 12 
    actions1      = [[],[20],[30],[80],[100],[20,30],[20,80],[30,80],[20,100],[30,100],[80,100],[20,30,80],[20,30,100],[20,80,100],[30,80,100],[20,30,80,100]]
    actions2      = [[],[30],[50],[90],[30,50],[50,50],[30,90],[50,90],[30,50,50],[30,50,90],[50,50,90],[30,50,50,90]]


    #initialize matricies of legal moves
    legal_actions1 = getLegalMoves(actions1)
    legal_actions2 = getLegalMoves(actions2)

    #initialize multi-dimensional Q-matricies with 0 values
    Q1 = initQ(legal_actions1)
    Q2 = initQ(legal_actions2)

    #print(Q1)

    # test_action = [[],[2],[3],[4],[2,3],[3,4],[2,4],[2,3,4]]
    # legal_actions_test = getLegalMoves(test_action)

    #start learning
    num_episodes = 20000
    epsilon = 0.1
    discount_factor = 0.995 
    alpha = 0.8

    # #print(Q1[[20,30]])
    # action_probabilities1 = epsilonGreedy(Q1, epsilon, [20,30]) 

    # print (action_probabilities1)

    rewards_1 = []
    rewards_2 = []
    for ith_episode in range(num_episodes): 
        if ith_episode % 2000 == 0 :
            epsilon *= 0.5
        #init environment
        environmet = env(boat_capacity, person_weight, luggage1.copy(), luggage2.copy())
        #state in our case is how much luggage each of agents has
        state = [luggage1.copy(),luggage2.copy()] # initially - all baggage
        print ("episode "+str(ith_episode))
        done = False
        episode_reward_1 = 0
        episode_reward_2 = 0
        while (not done) : 
            # get probabilities of all actions for the current state 
            cur_legal_act1,action_probabilities1 = epsilonGreedy(Q1, epsilon, state[0]) 
            cur_legal_act2,action_probabilities2 = epsilonGreedy(Q2, epsilon, state[1]) 

            # choose action according to the probability distribution 
            action1 = np.random.choice(cur_legal_act1, p = action_probabilities1) 
            action2 = np.random.choice(cur_legal_act2, p = action_probabilities2) 

            act1_str = ""
            act2_str = ""
            for act in action1:
                act1_str=str(act1_str)+str(act)+" , "

            for act in action2:
                act2_str=str(act2_str)+str(act)+" , "    
            # print ("States:")
            # print (state[0])
            # print (state[1])
            # print ("action 1: "+act1_str)
            # print ("action 2: "+act2_str)
            # take action and get reward, transit to next state 
            reward_boat, reward_pas1, reward_pas2, next_state, done = environmet.step(action1,action2) 
            episode_reward_1+=reward_pas1
            episode_reward_2+=reward_pas2


            # Update statistics 
            # stats.episode_rewards1[i_episode] += reward_pas1
            # stats.episode_rewards2[i_episode] += reward_pas2 

            # TD Update 
            best_next_q1 = max(Q1[get_iteratorQ(next_state[0],Q1)][1],key=itemgetter(1))[1]	 
            td_target1 = reward_pas1 + discount_factor * best_next_q1
            cur_it1 = get_iteratorQ(state[0],Q1)
            current_Qs1 = Q1[cur_it1][1]
            itc1 = 0
            cur_Q1 = 0
            for act,val in current_Qs1:
                if (all(elem in act for elem in action1) and all(elem in action1 for elem in act) and len(act)==len(action1)):
                    cur_Q1=val
                    break
                itc1+=1
            td_delta1 = td_target1 - cur_Q1 
            Q1[cur_it1][1][itc1]=(Q1[cur_it1][1][itc1][0],Q1[cur_it1][1][itc1][1]+alpha * td_delta1)


            best_next_q2 = max(Q2[get_iteratorQ(next_state[1],Q2)][1],key=itemgetter(1))[1]	 
            td_target2 = reward_pas2 + discount_factor * best_next_q2
            cur_it2 = get_iteratorQ(state[1],Q2)
            # print(cur_it2)
            # print(Q2[cur_it2])
            current_Qs2 = Q2[cur_it2][1]
            itc2 = 0
            cur_Q2 = 0
            for act,val in current_Qs2:
                if (all(elem in act for elem in action2) and all(elem in action2 for elem in act) and len(act)==len(action2)):
                    cur_Q2=val
                    break
                itc2+=1
            td_delta2 = td_target2 - cur_Q2
            Q2[cur_it2][1][itc2]=(Q2[cur_it2][1][itc2][0],Q2[cur_it2][1][itc2][1]+alpha * td_delta2)            

            # best_next_action2 = np.argmax(Q2[next_state[1]])
            # td_target2 = reward_pas2 + discount_factor * Q2[next_state[1]][best_next_action2] 
            # td_delta2 = td_target2 - Q2[state[1]][action2] 
            # Q2[state[1]][action2] += alpha * td_delta2             
                
            state = next_state.copy()    
        
        rewards_1.append(episode_reward_1)
        rewards_2.append(episode_reward_2)
    
    print(Q1)
    print(Q2)

    #plt.plot(episodes, rewards_1, 'r', episodes, rewards_2, 'b')
    line_up, = plt.plot(rewards_1, label='passenger 1')
    line_down, = plt.plot(rewards_2, label='passenger 2')
    plt.legend(handles=[line_up, line_down])
    plt.xlabel('episode')
    plt.ylabel('reward')
    plt.show()

# saved picture stands for:
# 10 000 epochs. Each 1000 epoch epsilon is multiplyed on 0.5, starting from 0.1
# the prefered sequence of actions:
# passenger 1
# first step:  [30, 100] 300+1000-100 = 1200 
# second step: [20, 80]  200+800      = 1000
#                                     = 2200
# passenger 2
# first step:  [30]      300-100      = 200
# second step: [50]      500          = 400
#                                     = 600
# one could see, that passenger 1 started dominating over the passenger 2 


# result after 20 000 epochs. Each 2000 epoch epsilon is multiplyed on 0.5, starting from 0.1
# the prefered sequence of actions:
# passenger 1:
#   step 1 : [20, 30]      200+300-100  = 400
#   step 2 : [100]         1000-100     = 900
#   step 3 : [80]          800          = 800
#                                       = 2100
# 
# passenger 2:
#   step 1 : [30, 50]      300+500-100  = 700  
#   step 2 : [50]          500-100      = 400
#   step 3 : [90]          900          = 900
#                                       = 2000
# 

#when agents are informed about the luggage of the others, then state contains luggage of others and tables are bigger
#implement that as well
